from mlxtend.classifier import MultiLayerPerceptron as MLP
from mlxtend.data import loadlocal_mnist
from mlxtend.preprocessing import shuffle_arrays_unison
from mlxtend.preprocessing import standardize
from openpyxl import load_workbook
from datetime import date
import random
import tkinter as tk
import numpy as np
import turtle
from turtle import Turtle, Screen
from PIL import Image, EpsImagePlugin, ImageOps

log_data = './log/log_data.xlsx'
wb = load_workbook(filename=log_data)
ws = wb['Planilha1']
EpsImagePlugin.gs_windows_binary = r'C:\Program Files\gs\gs9.52\bin\gswin64c.exe'

screen = Screen()
t = Turtle("turtle")
t.pensize(35)
t.speed(-1)
t.shapesize(2,2,2)
t.fillcolor("green")
size = (28, 28)
screen.title("D=deleta | SPAÇO=geraimg | MOUSDIR=sobeCaneta | MOUSMEIO=desceCaneta")

def dragging(x, y):
    t.ondrag(None)
    t.setheading(t.towards(x, y))
    t.goto(x, y)
    t.ondrag(dragging)

def rigthclick(x, y):
    t.fillcolor("red")
    t.penup()

def midleclick(x, y):
    t.fillcolor("green")
    t.pendown()

def erase():
    t.clear()

def imagearray():
    t.fillcolor("black")
    ts = turtle.getscreen()
    ts.getcanvas().postscript(file="number.eps")
    imgeps = Image.open("./number.eps").convert('L')
    imginvert = ImageOps.invert(imgeps)
    imgneg = imginvert.resize(size)
    imgarray = np.array(imgneg)
    imgarray1d = imgarray.flatten()
    print(imgarray1d)
    imgpng = Image.fromarray(imgarray)
    imgpng.save("number.png")
    t.fillcolor("red")
    t.penup()

    X_treino, y_treino = loadlocal_mnist(
        images_path='./data/train-images.idx3-ubyte',
        labels_path='./data/train-labels.idx1-ubyte'
    )
    X_treino, y_treino = shuffle_arrays_unison(
        (X_treino, y_treino), random_seed=1)

    X_teste, y_teste = loadlocal_mnist(
        images_path='./data/t10k-images.idx3-ubyte',
        labels_path='./data/t10k-labels.idx1-ubyte'
    )
    X_teste, y_teste = shuffle_arrays_unison((X_teste, y_teste), random_seed=1)

    X_treino_std, params = standardize(
        X_treino,
        columns=range(X_treino.shape[1]),
        return_params=True
    )

    X_teste_std = standardize(
        X_teste,
        columns=range(X_teste.shape[1]),
        params=params
    )

    hidden_layers_setup = 6
    l2_setup = 0.0
    l1_setup = 0.0
    epochs_setup = 5
    eta_setup = 0.1
    momentum_setup = 0.0
    decrease_const_setup = 0.0
    minibatches_setup = 100
    random_seed_setup = 1
    print_progress_setup = 3

    net = MLP(
        hidden_layers=[hidden_layers_setup],
        l2=l2_setup,
        l1=l1_setup,
        epochs=epochs_setup,
        eta=eta_setup,
        momentum=momentum_setup,
        decrease_const=decrease_const_setup,
        minibatches=minibatches_setup,
        random_seed=random_seed_setup,
        print_progress=print_progress_setup
    )

    net.fit(X_treino_std, y_treino)

    print('\n\nPrecisão Treino: %.2f%% | Precisão de teste: %.2f%%' %
          ((100 * net.score(X_treino_std, y_treino)),
           (100 * net.score(X_teste_std, y_teste))))

    ultimaLinha = ws.max_row + 1
    dataAtual = date.today()
    ws.cell(column=1, row=ultimaLinha, value=dataAtual)
    ws.cell(column=2, row=ultimaLinha, value=100 * net.score(X_treino_std, y_treino))
    ws.cell(column=3, row=ultimaLinha, value=100 * net.score(X_teste_std, y_teste))
    ws.cell(column=4, row=ultimaLinha, value=hidden_layers_setup)
    ws.cell(column=5, row=ultimaLinha, value=l2_setup)
    ws.cell(column=6, row=ultimaLinha, value=l1_setup)
    ws.cell(column=7, row=ultimaLinha, value=epochs_setup)
    ws.cell(column=8, row=ultimaLinha, value=eta_setup)
    ws.cell(column=9, row=ultimaLinha, value=momentum_setup)
    ws.cell(column=10, row=ultimaLinha, value=decrease_const_setup)
    ws.cell(column=11, row=ultimaLinha, value=minibatches_setup)
    ws.cell(column=12, row=ultimaLinha, value=random_seed_setup)
    ws.cell(column=13, row=ultimaLinha, value=print_progress_setup)
    wb.save(filename=log_data)
    wb.close()

    X_rand = X_teste[9999:]
    print('Dimensions: %s x %s' % (X_rand.shape[0], X_rand.shape[1]))

    img = X_rand[0].reshape(28, 28)

    print('Seu resultado foi: %d' % net.predict(standardize(X_rand, columns=range(X_rand.shape[1]), params=params)))
    X_rand[0] = [random.randint(0, 255) for x in X_rand[0]]

def main():
    turtle.listen()
    t.ondrag(dragging)
    turtle.onscreenclick(rigthclick, 3)
    turtle.onscreenclick(midleclick, 2)
    turtle.onkey(imagearray, "space")
    turtle.onkey(erase, "d")
    screen.mainloop()

main()